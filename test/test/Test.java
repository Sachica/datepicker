/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import datepicker_min.DatePicker;
import datepicker.ToolVista;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 *
 * @author Sachica - https://gitlab.com/Sachica [GitLab]
 * @contact sachikia27@gmail.com [Gmail]
 */
public class Test {
    private JFrame frame;
    private DatePicker dataPicker;
    
    public Test() throws Exception {
        this.frame = new JFrame("DataPicker Test");
        this.frame.setMinimumSize(new Dimension(300, 250));
        this.frame.setLocationRelativeTo(null);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        this.dataPicker = new DatePicker();

        this.init();
        this.frame.setVisible(true);
        
    }

    private void init() throws Exception {
        this.frame.getContentPane().setLayout(ToolVista.GBL);

        ToolVista.insert(this.frame.getContentPane(), this.dataPicker, 0, 0, 1, 1, 1, 1, GridBagConstraints.NONE, GridBagConstraints.FIRST_LINE_START, new Insets(5, 5, 5, 5), 0, 0);
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            try{
                new Test();
            }catch(Exception e){
                e.printStackTrace();
            }
        });
    }
}
