/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datepicker_min;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

/**
 *
 * @author Sachica - https://gitlab.com/Sachica [GitLab]
 * @contact sachikia27@gmail.com [Gmail]
 */
public class DatePicker extends JButton {

    private final JFrame frame;
    private final Calendar currentDate = Calendar.getInstance();
    private Calendar selectedDate;
    private Point positionCalendar;
    private final SimpleDateFormat dateFormat1 = new SimpleDateFormat("EEE, d MMM yyyy");
    private final SimpleDateFormat dateFormat2 = new SimpleDateFormat("yyyy / MM / dd");
    private final Cursor handCursor = new Cursor(Cursor.HAND_CURSOR);
    private final Color backgroundColor = new Color(Integer.parseInt("dfe6e9", 16));
    private final ButtonHover hover;
    private boolean show;

    private Calendar dateStart;
    private Calendar dateEnd;

    private final JButton btnBack;
    private final JButton btnDate;
    private final JButton btnNext;

    private final JComboBox<Object> cbMonths;
    private final JComboBox<Object> cbYears;

    private final String MONTHS[] = {"Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"};
    private final String DAYS[] = {"Dom.", "Lun.", "Mar.", "Mier.", "Jue.", "Vie.", "Sab."};
    private final JButton TEMPLATE[];

    /**
     * Inicializa las fechas de incio y fin con valores default <br>
     * Valore fecha inicio: new Date(1, 1, 1900)<br>
     * Valor fecha fin: new Date(1, 1, 2500)
     *
     * @throws Exception
     */
    public DatePicker() throws Exception {
        this(new Date(0, 0, 1), new Date(2500 - 1900, 0, 1));
    }

    /**
     *
     * @param dateStart Fecha inicio, desde donde se va a poder elegir un año en
     * el <code>JComboBox</code> de años<br>
     * @param dateEnd Fecha fin, hasta donde se va a poder elegir un año en el
     * <code>JComboBox</code> de años
     * @throws Exception
     */
    public DatePicker(Date dateStart, Date dateEnd) throws Exception {
        this.dateStart = Calendar.getInstance();
        this.dateStart.setTime(dateStart);
        this.dateEnd = Calendar.getInstance();
        this.dateEnd.setTime(dateEnd);

        this.positionCalendar = new Point(0, 30);
        this.frame = new JFrame() {
            @Override
            public void setVisible(boolean aFlag) {
                if (aFlag) {
                    onLoad();
                }
                super.setVisible(aFlag);
            }
        };
        this.frame.getContentPane().setBackground(Color.WHITE);
        this.frame.setMinimumSize(new Dimension(220, 160));
        this.frame.getContentPane().setLayout(ToolVista.GBL);
        this.frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JFrame.setDefaultLookAndFeelDecorated(true);
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        this.show = false;
        this.hover = new ButtonHover();
        this.selectedDate = Calendar.getInstance();
        this.TEMPLATE = new JButton[49];

        this.cbMonths = new JComboBox<>(MONTHS);
        this.cbYears = new JComboBox<>();

        this.btnBack = new JButton("<");
        this.btnDate = new JButton(this.dateFormat1.format(this.currentDate.getTime()));
        this.btnNext = new JButton(">");

        this.init();

        this.frame.setUndecorated(true);
        this.frame.setVisible(false);
        super.setVisible(true);
    }

    /*
        Cuando se pida mostrar el calendario se mira donde esta this y le suma la posición
        correspondiente a cada eje en asiganada en positionCalendar
    */
    private void onLoad() {
        frame.setLocation(super.getLocationOnScreen().x+this.positionCalendar.x, super.getLocationOnScreen().y+this.positionCalendar.y);
    }

    private void init() throws Exception {
        this.updateCbYear();
        /*
            JPanel para contener los paneles
            panelCombos, panelMonth
         */
        JPanel panelBackground = new JPanel(ToolVista.GBL);
        panelBackground.setOpaque(false);

        /*
            JPanel para contener el calendario
            TEMPLATE
         */
        JPanel panelMonth = new JPanel(new GridLayout(7, 7));
        panelMonth.setOpaque(false);

        /*
            JPanel para contener los JComboBox
            cbMonths, cbYears
         */
        JPanel panelCombos = new JPanel(ToolVista.GBL);
        panelCombos.setOpaque(false);
        panelCombos.setVisible(this.show);

        /*
            JPanel para contener los botones de control
            btnBack, btnDate, btnNext
         */
        JPanel panelButtons = new JPanel(ToolVista.GBL);
        panelButtons.setOpaque(false);


        /*
            Añade un ActionListener para cuando el JComboBox de años cambie,
            actualice la GUI con los datos referentes al año selecionado
         */
        this.cbYears.addActionListener((ActionEvent e) -> {
            this.currentDate.set(Calendar.YEAR, Integer.parseInt(this.cbYears.getSelectedItem().toString()));
            this.showCalendar(this.currentDate.getTime());
        });
        /*Personalización de JComboBox*/
        this.cbYears.setMaximumRowCount(7);
        this.cbYears.setFocusable(false);
        this.cbYears.setCursor(this.handCursor);
        ((JLabel) this.cbYears.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);

        /*
            Añade un ActionListener para cuando el JComboBox de meses cambie,
            actualice la GUI con los datos referentes al mes selecionado
         */
        this.cbMonths.addActionListener((ActionEvent e) -> {
            this.currentDate.set(Calendar.MONTH, this.cbMonths.getSelectedIndex());
            this.showCalendar(this.currentDate.getTime());
        });
        /*Personalización de JComboBox*/
        this.cbMonths.setMaximumRowCount(7);
        this.cbMonths.setFocusable(false);
        this.cbMonths.setCursor(this.handCursor);
        ((JLabel) this.cbMonths.getRenderer()).setHorizontalAlignment(SwingConstants.CENTER);

        /*
            Pequeña implementación que realiza la acción button:hover de css
            pero para este caso los JButton
         */
        this.hover.setEntered(this.backgroundColor);
        this.hover.setExited(Color.WHITE);

        /*
            Hasta que el usuario no seleccione un dia(JButton) del calendario 
            no se habra selecionado una fecha, por lo tanto se agrega
            un oyente a los botones para capturar el dia escogido y
            asi actualizar la fecha escogida(this.selectedDate)
         */
        Action a = new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!e.getActionCommand().isEmpty()) {
                    selectedDate.set(Calendar.DAY_OF_MONTH, Integer.parseInt(e.getActionCommand()));
                    selectedDate.set(Calendar.MONTH, currentDate.get(Calendar.MONTH));
                    selectedDate.set(Calendar.YEAR, currentDate.get(Calendar.YEAR));
                    btnDate.setText(dateFormat1.format(selectedDate.getTime()));
                    showCalendar(selectedDate.getTime());
                    frame.setVisible(false);
                    show = false;
                }
            }
        };

        /*Personaliza el calendario para un aspecto mas agradable*/
        for (int i = 0; i < this.TEMPLATE.length; i++) {
            this.TEMPLATE[i] = new JButton(i < 7 ? DAYS[i] : "");
            this.TEMPLATE[i].setEnabled(i > 6);
            this.TEMPLATE[i].setBackground(Color.WHITE);
            this.TEMPLATE[i].setForeground((i % 7) == 0 && i != 0 ? Color.RED : Color.BLACK);
            this.TEMPLATE[i].setBorder(null);
            this.TEMPLATE[i].setFocusable(false);
            this.TEMPLATE[i].setContentAreaFilled(false);
            this.TEMPLATE[i].setOpaque(true);
            this.TEMPLATE[i].setCursor(this.handCursor);
            this.TEMPLATE[i].setHorizontalAlignment(SwingConstants.CENTER);
            this.TEMPLATE[i].addActionListener(a);
        }

        /*Se añaden la plantilla de días o calendario(TEMPLATE) al panelMonth*/
        for (JButton btn : this.TEMPLATE) {
            panelMonth.add(btn);
        }

        /*
            Se carga por default la actual fecha en el calendario y se formatea
            para mostrarla en el btnDate
         */
        this.showCalendar(this.currentDate.getTime());
        this.btnDate.setText(this.dateFormat1.format(this.currentDate.getTime()));

        this.customButton(this.btnBack);
        /*
            Se añade un ActionListener a btnBack para retroceder un mes del 
            actual en la vista
         */
        this.btnBack.addActionListener((ActionEvent e) -> {
            backMonth();
        });

        this.customButton(this.btnDate);
        /*
            Se añade un ActionListener a btnDate para ocultar los botones
            btnBack y btnNext a su vez haciendo visible el panelCombos que
            contiene cbMonth y cbYears
         */
        this.btnDate.addActionListener((ActionEvent e) -> {
            this.btnBack.setVisible(this.show);
            this.btnNext.setVisible(this.show);
            panelMonth.setVisible(this.show);
            panelCombos.setVisible(!this.show);
            this.show = !this.show;
        });

        this.customButton(this.btnNext);
        /*
            Se añade un ActionListener a btnBack para retroceder un año del 
            actual en la vista
         */
        this.btnNext.addActionListener((ActionEvent e) -> {
            nextMonth();
        });

        /*
            Se añade un FocusListener para cuando la ventana(this.frame) pierda el foco
            se oculte a la vista
         */
        this.frame.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            }

            @Override
            public void focusLost(FocusEvent e) {
                frame.setVisible(false);
                btnBack.setVisible(true);
                btnNext.setVisible(true);
                panelCombos.setVisible(false);
                panelMonth.setVisible(true);
                currentDate.setTime(selectedDate.getTime());
                show = false;
                showCalendar(currentDate.getTime());
            }
        });

        this.customButton(this);
        super.addActionListener((e) -> {
            this.frame.setVisible(true);
        });

        /*Se agregan todos los componentes a su panel correspondiente*/
        ToolVista.insert(panelButtons, this.btnBack, 0, 0, 1, 0, 1, 1, GridBagConstraints.HORIZONTAL, GridBagConstraints.LINE_START, null, 0, 0);
        ToolVista.insert(panelButtons, this.btnDate, 1, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 5, 0, 5), 0, 0);
        ToolVista.insert(panelButtons, this.btnNext, 2, 0, 1, 0, 1, 1, GridBagConstraints.HORIZONTAL, GridBagConstraints.LINE_END, null, 0, 0);

        ToolVista.insert(panelBackground, panelMonth, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, null, 0, 0);

        ToolVista.insert(panelCombos, this.cbMonths, 0, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, new Insets(0, 0, 0, 20), 0, 0);
        ToolVista.insert(panelCombos, this.cbYears, 1, 0, 0, 0, 1, 1, GridBagConstraints.NONE, GridBagConstraints.CENTER, null, 15, 0);

        ToolVista.insert(panelBackground, panelCombos, 0, 0, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, new Insets(0, 0, 100, 0), 0, 0);

        ToolVista.insert(this.frame.getContentPane(), panelButtons, 0, 0, 1, 0, 1, 1, GridBagConstraints.HORIZONTAL, GridBagConstraints.PAGE_START, new Insets(5, 5, 0, 5), 0, 0);
        ToolVista.insert(this.frame.getContentPane(), panelBackground, 0, 1, 1, 1, 1, 1, GridBagConstraints.BOTH, GridBagConstraints.CENTER, new Insets(5, 5, 5, 5), 0, 0);
    }

    /*
        Actualiza la GUI respecto al parametro Date pasado
     */
    private void showCalendar(Date date) {
        /*Resetea texto y remueve acción button:hover*/
        for (int i = 7; i < this.TEMPLATE.length; i++) {
            this.TEMPLATE[i].setText("");
            this.TEMPLATE[i].removeMouseListener(this.hover);
        }

        Calendar temp = Calendar.getInstance();
        temp.setTime(date);
        /*Con esto me puedo ubicar en el dia de la semana que comenzo el mes*/
        temp.set(Calendar.DAY_OF_MONTH, 1);

        /*
            Comienzo en el TEMPLATE 6 posiciones despues debido a que en estas
            posiciones estan los dias de la semana, ademas de esto le sumo el dia
            de la semana desde donde comienza el mes
         */
        for (int i = temp.get(Calendar.DAY_OF_WEEK) + 6, k = 1; k <= temp.getActualMaximum(Calendar.DATE); i++) {
            this.TEMPLATE[i].setText("" + (k++));
            this.TEMPLATE[i].addMouseListener(this.hover);
        }

        temp.set(Calendar.DAY_OF_MONTH, this.currentDate.get(Calendar.DAY_OF_MONTH));

        /*
            Si ya hay fecha seleccionada(selectedDate) tomo el dia para mantenerlo 
            en la vista, sino sigo con el dia la fecha por default
         */
        if (!this.selectedDate.equals(this.currentDate)) {
            temp.set(Calendar.DAY_OF_MONTH, this.selectedDate.get(Calendar.DAY_OF_MONTH));
        }

        /*Actualizo componentes*/
        this.cbYears.setSelectedItem(temp.get(Calendar.YEAR));
        this.cbMonths.setSelectedIndex(temp.get(Calendar.MONTH));
        this.btnDate.setText(this.dateFormat1.format(temp.getTime()));
        super.setText(this.dateFormat2.format(this.selectedDate.getTime()));
    }

    /*
        Retrocede un mes del actual y actualiza la GUI para mostrar la información
        correspondiente del mes retrocedido
     */
    private void backMonth() {
        this.currentDate.set(Calendar.MONTH, this.currentDate.get(Calendar.MONTH) - 1);
        this.showCalendar(this.currentDate.getTime());
    }

    /*
        Avanza un mes del actual y actualiza la GUI para mostrar la información
        correspondiente del mes avanzado
     */
    private void nextMonth() {
        this.currentDate.set(Calendar.MONTH, this.currentDate.get(Calendar.MONTH) + 1);
        this.showCalendar(this.currentDate.getTime());
    }

    /*
        Inicializa el JComboBox de años de acuerdo al rango entre 
        this.dateStart y this.dateEnd siendo ambas incluidas
     */
    private void updateCbYear() {
        this.cbYears.removeAllItems();
        for (int i = this.dateStart.get(Calendar.YEAR); i <= this.dateEnd.get(Calendar.YEAR); i++) {
            this.cbYears.addItem(i);
        }
    }

    /*
        Personaliza los JButton [btnBack, btnDate, btnNext] para un mejor aspecto
     */
    private void customButton(JButton btn) {
        btn.setFocusable(false);
        btn.setContentAreaFilled(false);
        btn.setOpaque(true);
        btn.setBackground(this.backgroundColor);
        btn.setCursor(this.handCursor);
    }

    /**
     * Retorna la fecha seleccionada en un objeto de tipo
     * <code>Calendar</code>.<br>
     * Si no se ha elegido fecha o cambiado mediante el metodo
     * <code>setSelectedDate(Calendar calendar)</code> por default es la fecha
     * actual
     *
     * @return <code>Calendar</code> - fecha selecionada
     */
    public Calendar getSelectedDate() {
        return selectedDate;
    }

    /**
     * Cambia la fecha selecionada y actualiza la GUI para mostrar los datos
     * relacionados a la nueva fecha
     *
     * @param selectedDate nueva fecha selecionada
     * @throws java.lang.Exception fecha no valida de acuerdo a las fechas
     * establecidas <code>dateStart</code> y <code>dateEnd</code>
     */
    public void setSelectedDate(Calendar selectedDate) throws Exception {
        if (selectedDate.getTimeInMillis() > this.dateEnd.getTimeInMillis()
                || selectedDate.getTimeInMillis() < this.dateStart.getTimeInMillis()) {
            throw new Exception("Fecha no valida, esta fecha sobrepasa los limites de las fechas de inicio y fin establecidas");
        }
        this.selectedDate = selectedDate;
        this.showCalendar(this.selectedDate.getTime());
    }

    /**
     *
     * @return fecha de inicio
     */
    public Calendar getDateStart() {
        return dateStart;
    }

    /**
     * Cambiar la fecha incio actual y actualiza el <code>JComboBox</code> de
     * años
     *
     * @param dateStart nueva fecha de inicio
     * @throws Exception fecha no valida si el param <code>dateStart</code> es
     * mayor a la fecha fin
     */
    public void setDateStart(Calendar dateStart) throws Exception {
        if (dateStart.getTimeInMillis() > this.dateEnd.getTimeInMillis()) {
            throw new Exception("Fecha inicio no puede ser mayor que la fecha final");
        }
        this.dateStart = dateStart;
        this.updateCbYear();
    }

    /**
     *
     * @return fecha de fin
     */
    public Calendar getDateEnd() {
        return dateEnd;
    }

    /**
     * Cambiar la fecha fin actual y actualiza el <code>JComboBox</code> de años
     *
     * @param dateEnd nueva fecha de inicio
     * @throws Exception fecha no valida si el param <code>dateEnd</code> es
     * menor a la fecha fin
     */
    public void setDateEnd(Calendar dateEnd) throws Exception {
        if (dateEnd.getTimeInMillis() < this.dateStart.getTimeInMillis()) {
            throw new Exception("Fecha final no puede ser menor que la fecha de inicio");
        }
        this.dateEnd = dateEnd;
        this.updateCbYear();
    }

    /**
     * Retorna la posición relativa a this donde se encuentra ubicado el calendario
     * @return 
     */
    public Point getPositionCalendar() {
        return positionCalendar;
    }

    /**
     * Cambia la posición relativa a this donde se encontrara ubicado el calendario
     * @param positionCalendar nueva posición del calendario
     */
    public void setPositionCalendar(Point positionCalendar) {
        this.positionCalendar = positionCalendar;
    }
}

class ButtonHover implements MouseListener {
    private Color entered;
    private Color exited;
    private Color clicked;
    private Color pressed;
    private Color released;

    public ButtonHover() {
    }

    public ButtonHover(Color entered, Color exited, Color clicked, Color pressed, Color released) {
        this.entered = entered;
        this.exited = exited;
        this.clicked = clicked;
        this.pressed = pressed;
        this.released = released;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(this.clicked!=null)((JButton) e.getSource()).setBackground(this.clicked);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(this.pressed!=null)((JButton) e.getSource()).setBackground(this.pressed);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(this.released!=null)((JButton) e.getSource()).setBackground(this.released);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(this.entered!=null)((JButton) e.getSource()).setBackground(this.entered);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(this.exited!=null)((JButton) e.getSource()).setBackground(this.exited);
    }

    public Color getEntered() {
        return entered;
    }

    public void setEntered(Color entered) {
        this.entered = entered;
    }

    public Color getExited() {
        return exited;
    }

    public void setExited(Color exited) {
        this.exited = exited;
    }

    public Color getClicked() {
        return clicked;
    }

    public void setClicked(Color clicked) {
        this.clicked = clicked;
    }

    public Color getPressed() {
        return pressed;
    }

    public void setPressed(Color pressed) {
        this.pressed = pressed;
    }

    public Color getReleased() {
        return released;
    }
}

class ToolVista {
    public static final GridBagLayout GBL = new GridBagLayout();
    private static final GridBagConstraints GBC = new GridBagConstraints();
    
    /*
        Documentación para aprender GridBagLayout: 
        https://docs.oracle.com/javase/tutorial/uiswing/layout/gridbag.html
     */
    public static void insert(Container contenedor, Component componente, int gridx, int gridy, double weightx, double weighty, int gridwidth, int gridheight, int fill, int anchor, Insets insets, int ipadx, int ipady) throws Exception{
        if(!(contenedor.getLayout() instanceof GridBagLayout))
            throw new Exception("El contenerdor ["+contenedor.getClass()+"] no tiene establecido el layout aceptado ["+GridBagLayout.class+"]");
        if(insets == null){
            insets = new Insets(0, 0, 0, 0);
        }
        ToolVista.GBC.gridx = gridx;
        ToolVista.GBC.gridy = gridy;
        ToolVista.GBC.weightx = weightx;
        ToolVista.GBC.weighty = weighty;
        ToolVista.GBC.gridwidth = gridwidth;
        ToolVista.GBC.gridheight = gridheight;
        ToolVista.GBC.fill = fill;
        ToolVista.GBC.anchor = anchor;
        ToolVista.GBC.insets = insets;
        ToolVista.GBC.ipadx = ipadx;
        ToolVista.GBC.ipady = ipady;
        contenedor.add(componente, ToolVista.GBC);
        reset();
    }
    
    private static void reset(){
        ToolVista.GBC.anchor = GridBagConstraints.CENTER;
        ToolVista.GBC.fill = GridBagConstraints.NONE;
        ToolVista.GBC.gridheight = 1;
        ToolVista.GBC.gridwidth = 1;
        ToolVista.GBC.insets.bottom = 0;
        ToolVista.GBC.insets.top = 0;
        ToolVista.GBC.insets.left = 0;
        ToolVista.GBC.insets.right = 0;
        ToolVista.GBC.weightx = 0;
        ToolVista.GBC.weighty = 0;
        ToolVista.GBC.ipadx = 0;
    }
}