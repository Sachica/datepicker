/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datepicker;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.JButton;

/**
 *
 * @author Sachica - https://gitlab.com/Sachica [GitLab]
 * @contact sachikia27@gmail.com [Gmail]
 */
public class ButtonHover implements MouseListener {
    private Color entered;
    private Color exited;
    private Color clicked;
    private Color pressed;
    private Color released;

    public ButtonHover() {
    }

    public ButtonHover(Color entered, Color exited, Color clicked, Color pressed, Color released) {
        this.entered = entered;
        this.exited = exited;
        this.clicked = clicked;
        this.pressed = pressed;
        this.released = released;
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        if(this.clicked!=null)((JButton) e.getSource()).setBackground(this.clicked);
    }

    @Override
    public void mousePressed(MouseEvent e) {
        if(this.pressed!=null)((JButton) e.getSource()).setBackground(this.pressed);
    }

    @Override
    public void mouseReleased(MouseEvent e) {
        if(this.released!=null)((JButton) e.getSource()).setBackground(this.released);
    }

    @Override
    public void mouseEntered(MouseEvent e) {
        if(this.entered!=null)((JButton) e.getSource()).setBackground(this.entered);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        if(this.exited!=null)((JButton) e.getSource()).setBackground(this.exited);
    }

    public Color getEntered() {
        return entered;
    }

    public void setEntered(Color entered) {
        this.entered = entered;
    }

    public Color getExited() {
        return exited;
    }

    public void setExited(Color exited) {
        this.exited = exited;
    }

    public Color getClicked() {
        return clicked;
    }

    public void setClicked(Color clicked) {
        this.clicked = clicked;
    }

    public Color getPressed() {
        return pressed;
    }

    public void setPressed(Color pressed) {
        this.pressed = pressed;
    }

    public Color getReleased() {
        return released;
    }
}
