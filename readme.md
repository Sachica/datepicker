# DatePicker
Complemento grafico funcional que permite al usuario de forma comoda elegir una fecha, componente desarrollado completamente en Java.

## Información
Pese a que ya existen librerias desarrolladas que ofrecen esta funcionalidad me propuse como reto poder hacer mi propia implementación de este complemento no sin antes tener una base grafica de la cual guiarme.

La base que tome fue el elemento `<input type="date">` del lenguaje de etiquetado HTML:<br>
##### Vista inicial
![Elemento input de tipo date HTML](imgs/input-date.PNG)
##### Vista de calendario
![Elemento input de tipo date primera vista HTML](imgs/input-date-days.PNG)
##### Vista de meses y años
![Elemento input de tipo date segunda vista HTML](imgs/input-date-month-years.PNG)<br>
Teniendo en cuenta las vistas y el comportamiento de este componente inicie con el desarrollo de la misma, obteniendo asi, los siguientes resultados de cada vista respectivas a la base dada.
## Resultados
##### Vista inicial
![DataPicker](imgs/data-picker.PNG)
##### Vista de calendario
![DataPicker primera vista](imgs/data-picker-days.PNG)
##### Vista de meses y años
![DataPicker segunda vista](imgs/data-picker-month-years.PNG)<br>
Se puede observar una igualdad un tanto notable ademas de su similar comportamiento al momento de mostrar o ocultar componentes para pasar de vistas, ademas claro de que esta ventana `JFrame` en la cual se muestra el calendario siempre sera relativa a la ubicación del `DatePicker`.

## Uso
Para el uso de este componente cabe resaltar que la clase `DatePicker` es basicamente un `JButton` ya que extiende de este mismo, esto con el fin de hacer mas sencilla la integración a cualquier GUI como veremos a continuación. Recomiendo usar la clase `DatePicker` especificamente del paquete [_datepicker_min_](src/datepicker_min) ya que asi solo añadiran una clase a su proyecto y no tres como en el paquete [_datepicker_](src/datepicker).<br>
Sin nada mas que decir, para su sencillo uso primeramente crearemos las instancias de un `JFrame` y `DatePicker`.
```
JFrame frame = new JFrame("DatePicker Test");
DatePicker datePicker = new DatePicker();
```
Luego solo añadiremos el `datePicker` al `frame`, tal cual como hariamos con cualquier otro componente y hacemos visible el `frame`.
```
frame.add(datePicker);
frame.setVisible(true);
```
Y listo, el `datePicker` se encontrara dentro del `frame` listo para su uso, he de mencionar que este ejemplo es solo para mostrar como integrar el componente, luego se tendra que personalizar el `JFrame` o `Container` donde se encontraria el componente para que su visualización sea mas atractiva, recordar tambien que se puede personalizar el mismo `datePicker` ya que este es un  `JButton`.

## Feedback
Estare atento a cualquier inquietud o sugerencia respecto al componente, con el fin de poder mejorarlo o reparar fallos encontrados, me encontraria agradecido de cualquier critica respecto al codigo esto con el fin de poder mejorar como desarrollador.

Espero les sirva de ayuda este componente :)

